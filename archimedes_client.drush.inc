<?php
/**
 * @file
 * Drush integration for the archimedes module.
 */

use \Drupal\archimedes_client\Report;

/**
 * Implements hook_drush_command().
 */
function archimedes_client_drush_command()
{
  $items['arch-report'] = array(
    'description' => "Display an Archimedes Client report.",
    'options' => array(
      'format' => 'Rendered (human readable), JSON, or encrypted JSON',
    ),
    'examples' => array(
      'drush arch-report' => 'Display a report.',
      'drush arch-report json' => 'Display a report in JSON format.',
      'drush arch-report encrypted > report.enc' => 'Save an encypted report to file.',
    ),
    'aliases' => array('ar'),
  );
  $items['arch-send'] = array(
    'description' => "Send an Archimedes Client report.",
    'options' => array(
      'method' => 'Email or HTTP',
      'location' => 'Email address or URL to send the report to (depending on chosen method)',
    ),
    'examples' => array(
      'drush arch-send' => 'Send a report using the configured method.',
      'drush arch-send http' => 'Send a report using HTTP.',
    ),
    'aliases' => array('as'),
  );
  return $items;
}

/**
 * Check that the hostname has been provided to Drush using the -l flag.
 * @return boolean
 */
function _archimedes_client_require_drush_hostname()
{
  global $base_root;
  return $base_root != 'http://default';
}


/**
 * Drush command callback for arch-report
 *
 * Generates and dislpays a report in the chosen (or default) format
 *
 * @param  string $format
 *   Choice of report format (Rendered, JSON, or Encrypted)
 * @param  string $location
 *   The report endpoint location (e.g. Email or URL)
 */
function drush_archimedes_client_arch_report($format = 'rendered', $location = FALSE)
{
  if (!_archimedes_client_require_drush_hostname())
  {
    drush_set_error('archimedes_client_hostname_not_set', 'Cannot send report: could not find a valid hostname for this site.');
    return;
  }

  drush_print($location);

  $report = new Report();
  switch (strtolower($format))
  {
    case 'rendered':
    case 'render':
    case 'human':
    case 'human-readable':
    default:
      $data = $report->getRendered();
      $rows = array(array('Item','Value'));
      foreach ($data as $k => $v)
        $rows[] = array($k, $v);
        drush_print_table($rows, true);
      break;

    case 'json':
      drush_print($report->getJSON());
      break;

    case 'json-pretty':
    case 'pretty-json':
      drush_print($report->getJSON(true));
      break;

    case 'encrypted':
    case 'encrypted-json':
    case 'ejson':
      drush_print($report->getEncrypted());
      break;
  }
}

/**
 * Drush command callback for arch-send
 *
 * Generates and sends a report using the configured or given format and endpoint location
 *
 * @param  string $method
 *   Reporting method to use (e.g. Email or HTTP)
 * @param  string $location
 *   Endpoint location to use (e.g. an email address or URL)
 */
function drush_archimedes_client_arch_send($method = '', $location = '')
{
  if (!_archimedes_client_require_drush_hostname())
  {
    drush_set_error('archimedes_client_hostname_not_set', 'Hostname was not specified. Try adding \'-l http://example.com\'');
    return;
  }

  if ($method && !in_array(strtolower($method), array('email','http')))
  {
    drush_set_error('archimedes_client_send_invalid_method','Invalid report send method!');
    return;
  }

  $config = \Drupal::config('archimedes_client.settings');

  if (!$method) $method = $config->get('server.method');
  if (!$location && $method == 'email') $location = $config->get('server.email');
  if (!$location && $method == 'http') $location = $config->get('server.url');

  drush_print("Sending $method report to $location");
  $report = new Report();
  $r = $report->send($method, $location);

  if ($r === TRUE) {
    drush_print('Success!');
  }
  else {
    // drush_set_error('archimedes_client_send_fail', $r);
    \Drupal::logger('archimedes_client')->error('Could not send report via @method method. Reason: %error_msg',
      array(
        '@method'=> $method,
        '%error_msg' => $r
    ));
  }

}
