<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\Themes
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * Themes
 *
 * Currently installed and enabled themes
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class Themes extends Item
{
  /**
   * Gets an array of themes, keyed numerically
   *
   * @return array
   *   Themes
   */
  public function get()
  {
    $themes = array();

    foreach (array() as $theme)
    {
      $info = Yaml::parse($theme->getPathname());
      $themes[] = array(
        'Theme'       => $theme,
        'Name'        => (isset($info['name']) ? $info['name'] : ''),
        'Description' => (isset($info['description']) ? $info['description'] : ''),
        'Version'     => (isset($info['version']) ? $info['version'] : ''),
        'Project'     => (isset($info['project']) ? $info['project'] : ''),
        'Url'         => (isset($info['project status url']) ? $info['project status url'] : ''),
      );
    }
    return $themes;
  }

  /**
   * Gets a string denoting the number of themes installed
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    $count = count($this->get());
    $p = ($count == 1) ? 'theme' : 'themes';
    return "$count $p";
  }
}
