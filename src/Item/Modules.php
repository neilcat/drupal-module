<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\Modules
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
use Drupal\Core\Extension\ModuleHandler;
use Symfony\Component\Yaml\Yaml;

/**
 * Modules
 *
 * Currently installed and enabled modules
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class Modules extends Item
{
  /**
   * Gets an array of modules, keyed numerically
   *
   * @return array
   *   Modules
   */
  public function get()
  {
    $handler = \Drupal::moduleHandler();

    $modules = array();
    foreach ($handler->getModuleList() as $name => $module)
    {
      $info = Yaml::parse($module->getPathname());
      $modules[] = array(
        'Module'      => $name,
        'Name'        => (isset($info['name']) ? $info['name'] : ''),
        'Description' => (isset($info['description']) ? $info['description'] : ''),
        'Package'     => (isset($info['package']) ? $info['package'] : ''),
        'Version'     => (isset($info['version']) ? $info['version'] : ''),
        'Core'        => (isset($info['core']) ? $info['core'] : ''),
      );
    }
    return $modules;
  }

  /**
   * Gets a string denoting the number of modules installed
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    return "".count($this->get())." modules";
  }
}
