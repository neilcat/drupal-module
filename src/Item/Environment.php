<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\Environment
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
/**
 * Environment
 *
 * The current environment, as set by the Environment module.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class Environment extends Item
{
  /**
   * Gets an array of the current environment name and label
   *
   * @return array
   *   Environment name and label
   */
  public function get()
  {
    // TODO: ... when Environment is upgraded to Drupal 8
    // $env = environment_current(NULL,NULL,TRUE);
    // $env = array_shift($env);
    return array('Label'=>'Unknown', 'Name'=>'unknown');
  }

  /**
   * Gets a string denoting the current environment and it's label
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    extract($this->get());
    return "$Label ($Name)";
  }
}
