<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\SiteKey
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * SiteKey
 *
 * The random key that distinguishes this site on the Archimedes server.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class SiteKey extends Item
{
  /**
   * Gets the site key
   *
   * @return string
   *   Site key
   */
  public function get()
  {
    $config = \Drupal::config('archimedes_client.settings');
    return $config->get('report.key');
  }
}
