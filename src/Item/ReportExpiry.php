<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\ReportExpiry
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
/**
 * ReportExpiry
 *
 * Expiry time of the report, to prevent replay-attacks.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class ReportExpiry extends Item
{
  /**
   * Gets the expiry time
   *
   * @return integer
   *   Expiry time as a UNIX timestamp
   */
  public function get()
  {
    $config = \Drupal::config('archimedes_client.settings');
    return time() + $config->get('report.expiry');
  }

  /**
   * Gets the expiry time formatted to RFC 2822
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    return date('r', $this->get());
  }
}
