<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\Nodes
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
/**
 * Nodes
 *
 * Counts of all nodes and revisions (no their content).
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class Nodes extends Item
{
  /**
   * Gets an array of the node and revisions counts
   *
   * @return array
   *   Counts
   */
  public function get()
  {
    $nodes = db_query("SELECT COUNT(nid) as count FROM {node}")->fetchField();
    $revisions = db_query("SELECT COUNT(vid) as count FROM {node_revision}")->fetchField();
    return array('Nodes'=>$nodes, 'Revisions'=>$revisions);
  }

  /**
   * Gets a string denoting the number of nodes and revisions on the side
   *
   * @return string
   *   HTML markup
   */
  public function render()
  {
    extract($this->get());
    $p1 = ($Nodes == 1) ? '' : 's';
    $p2 = ($Revisions == 1) ? '' : 's';
    $Nodes = ($Nodes == 0) ? '0' : $Nodes;
    $Revisions = ($Revisions == 0) ? '0' : $Revisions;
    return "$Nodes node{$p1}, $Revisions revision{$p2}";
  }
}
