<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\ReportType
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * ReportType
 *
 * Type of system sending the report - in this case Drupal.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class ReportType extends Item
{
  /**
   * Gets the machine name of this report type
   *
   * @return string
   *   Report type
   */
  public function get()
  {
    // Fixed value
    return 'drupal8';
  }

  /**
   * Gets a human readable name for this report type
   *
   * @return string
   *   Report type
   */
  public function render()
  {
    // Fixed value
    return 'Drupal 8';
  }
}
